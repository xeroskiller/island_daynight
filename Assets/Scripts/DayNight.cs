﻿using UnityEngine;
using System.Collections;

public class DayNight : MonoBehaviour {

    public float minsInDay;

    private float _minsInDay = 1.0f;
    private float _curAngle = 0.0f;
    private float _degreesPerSecond = 6.0f;

    void Start()
    {
        if (minsInDay != _minsInDay)
            _minsInDay = minsInDay;

        _degreesPerSecond = 360.0f / (_minsInDay * 60.0f);
        _curAngle = 0.0f;
    }

    void Update()
    {
        float angleRotationStep = _degreesPerSecond * Time.deltaTime;
        _curAngle += angleRotationStep;

        if (_curAngle >= 360.0f)
            _curAngle -= 360.0f;

        if (minsInDay != _minsInDay)
        {
            _minsInDay = minsInDay;
            _degreesPerSecond = 360.0f / (_minsInDay * 60.0f);
        }

        transform.eulerAngles = new Vector3(_curAngle, 0.0f, 0.0f);
    }

}
